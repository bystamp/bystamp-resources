# Resources

Welcome to the BYSTAMP resources project.

You will find all the resources you will need to implement the KEYMO SDK in your applications

For more information on how and where to use them, please refer to the [documentation](https://docs.bystamp.tech)

## Mockups

This section contains the [ADOBE XD](https://www.adobe.com/fr/products/xd.html) mockups for the mobile applications

## Graphics

Here you will find the images and icons you can use freely in your application.
